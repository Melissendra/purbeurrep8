#PurBeurre OpenClassRooms' project 8
With this project, we allow the user to search healthier products with the help of openfoodfacts api. The user can create an account where he could save all his favorite product.

##Appplication's address
https://purbeurre8-gaelle.herokuapp.com/

#Project installation
Clone this repository on your local machine.
- Go in your the repository.
- In purBeurre directory create your virtualenvironment:
    - ` python3 -m venv venv`
    - ` source venv/bin/activate`
    - ``pip install -r requirements.txt``
- Create a .env file where you put all your settings for the database with that variables:
    - '
    DJANGO_ALLOWED_HOSTS="localhost 127.0.0.1[::1]"
    POSTGRES_ENGINE=django.db.backends.postgresql
    POSTGRES_DB=database_name
    POSTGRES_USER=username
    POSTGRES_PASSWORD=your_password
    POSTGRES_HOST=localhost
    POSTGRES_PORT=5432
    DEBUG=True
    '
- Create a postgresql database and user on your local machine
- Do: 
    - ``python manage.py migrate``
- For filling the database:
    - ``python manage.py initdb``
- To load the application's home page
    - ` python manage.py runserver`

