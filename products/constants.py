URL = 'https://fr.openfoodfacts.org/cgi/search.pl'

CATEGORIES_LIST = [
    "pizza",
    "yaourts",
    "pate a tartiner",
    "aperitifs",
    "charcuterie",
    "conserves",
    "fromages",
    "snacks",
    "plats préparés"
]

TAGS = [
    'product_name',
    'nutrition_grade_fr',
    'image_url',
    'url',
    'categories',
]

KEEP_DATA_NUTRIMENTS = [
    'fat_100g',
    'saturated-fat_100g',
    'sugars_100g',
    'salt_100g',
]

KEEP_DATA_NUTRIMENTS_OPTIONS = [
    'salt',
    'sugars',
    'fat',
    'saturated-fat',
]
