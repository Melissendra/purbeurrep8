from django.db import models
from purBeurre.settings import AUTH_USER_MODEL


class Category(models.Model):
    """ Categories table """
    category_name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.category_name


class Product(models.Model):
    """ Models for all  the products """
    product_name = models.CharField(max_length=255)
    nutrition_grades = models.CharField(max_length=2)
    fat = models.CharField(max_length=30)
    fat_100g = models.DecimalField(max_digits=3, decimal_places=1)
    saturated_fat = models.CharField(max_length=30)
    saturated_fat_100g = models.DecimalField(max_digits=3, decimal_places=1)
    sugars = models.CharField(max_length=30)
    sugars_100g = models.DecimalField(max_digits=3, decimal_places=1)
    salt = models.CharField(max_length=30)
    salt_100g = models.DecimalField(max_digits=30, decimal_places=1)
    image_url = models.URLField()
    url = models.URLField()

    categories = models.ManyToManyField(Category, related_name="products")

    def __str__(self):
        return self.product_name


class Favorite(models.Model):
    """ Table where the user's favorite products will be saved """
    product = models.ForeignKey(
        Product,
        related_name='product',
        on_delete=models.CASCADE
    )
    substitute = models.ForeignKey(
        Product,
        related_name='substitute',
        on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        AUTH_USER_MODEL, on_delete=models.CASCADE
    )

    class Meta:
        """ Class to forbid duplicate product """
        unique_together = (('product_id', 'substitute_id', 'user_id'),)
