from django.test import TestCase
from django.urls import reverse

from ..models import Favorite


class ProductSearchTest(TestCase):
    fixtures = ['test_data.json']

    def test_success(self):
        """ Search returns something"""
        response = self.client.get(
            reverse('products:search'),
            {'product_search': 'Nutella'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 9)

    def test_return_nothing(self):
        """ Product not found in the database """
        response = self.client.get(
            reverse('products:search'),
            {'product_search': 'error'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 0)


class ResultTest(TestCase):
    fixtures = ['test_data.json']

    def test_product(self):
        """ Query returns list of 6 better products """
        response = self.client.get(
            reverse('products:results',
                    kwargs={'pk': 7369})
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context_data['object_list']), 6)


class SaveTest(TestCase):
    """ we test the favorites registery """
    fixtures = ['test_data.json']

    def test_save_substitute(self):
        """ Testing if all the conditions are ok """
        self.client.login(
            username='melissendra77@gmail.com',
            password="Gregory2310"
        )
        self.assertEqual(Favorite.objects.all().count(), 3)
