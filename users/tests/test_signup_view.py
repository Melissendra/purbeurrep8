from django.test import TestCase
from django.urls import resolve

from ..views import SignUpView


class TestSignUpView(TestCase):

    def test_signup_view(self):
        view = resolve('/users/signup/')
        self.assertEqual(
            view.func.__name__, SignUpView.as_view().__name__
        )
