from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
# from selenium import webdriver
# from selenium.webdriver.common.desired_capabilities import \
#     DesiredCapabilities


class LoginSeleniumTest(StaticLiveServerTestCase):
    """ Test of the login to the user's point of view"""
    fixtures = ["test_data.json"]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        # cls.selenium = webdriver.Remote(
        #     command_executor="http://selenium-hub:4444/wd/hub",
        #     desired_capabilities=DesiredCapabilities.CHROME
        # )
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def testLogin(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/users/login/'))

        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys("melissendra77@gmail.com")
        password_input = self.selenium.find_element_by_name('password')
        password_input.send_keys("Gregory2310")
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
